;;;; Copyright 2009 Vitaly Mayatskikh <v.mayatskih@gmail.com>
;;;;
;;;; This file is a part of CL-Perfcounters
;;;;
;;;; Performance counters are special hardware registers available on most modern
;;;; CPUs. These registers count the number of certain types of hw events: such
;;;; as instructions executed, cachemisses suffered, or branches mis-predicted -
;;;; without slowing down the kernel or applications. These registers can also
;;;; trigger interrupts when a threshold number of events have passed - and can
;;;; thus be used to profile the code that runs on that CPU.
;;;;
;;;; The Linux Performance Counter subsystem provides an abstraction of these
;;;; hardware capabilities. It provides per task and per CPU counters, counter
;;;; groups, and it provides event capabilities on top of those.  It
;;;; provides "virtual" 64-bit counters, regardless of the width of the
;;;; underlying hardware counters.
;;;;
;;;; CL-Perfcounters is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; CL-Perfcounters is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defpackage :perfcounters
  (:use :cl :cffi)
  (:export :with-performance-counters :time+))

(in-package :perfcounters)

;; x86-64: 298
;; x86:    336
;; ppc:    319
;; s390:   331
(progn
  (defconstant +syscall+ 298)
  (defconstant +pr-task-perf-counters-disable+ 31)
  (defconstant +pr-task-perf-counters-enable+ 32))

(defcstruct perf-counter-attr
  (type			:unsigned-int)
  (size			:unsigned-int)
  (config		:unsigned-long-long)
  (sample-period/freq   :unsigned-long-long)
  (sample-type          :unsigned-long-long)
  (read_format          :unsigned-int)
  (flags                :unsigned-long-long)
  (wakeup-events        :unsigned-int)
  (reserved-2           :unsigned-int)
  (reserved-3           :unsigned-long-long))

(progn
  (defconstant +perf-type-hardware+ 0)
  (defconstant +perf-type-software+ 1)
  (defconstant +perf-type-tracepoint+ 2)
  (defconstant +perf-type-hw-cache+ 3)

;; Common hardware events, generalized by the kernel:
  (defconstant +perf-count-hw-cpu-cycles+ 0)
  (defconstant +perf-count-hw-instructions+ 1)
  (defconstant +perf-count-hw-cache-references+ 2)
  (defconstant +perf-count-hw-cache-misses+ 3)
  (defconstant +perf-count-hw-branch-instructions+ 4)
  (defconstant +perf-count-hw-branch-misses+ 5)
  (defconstant +perf-count-hw-bus-cycles+ 6)

;; Special "software" counters provided by the kernel, even if the hardware
;; does not support performance counters. These counters measure various
;; physical and sw events of the kernel (and allow the profiling of them as
;; well):
  (defconstant +perf-count-sw-cpu-clock+ 32)
  (defconstant +perf-count-sw-task-clock+ 33)
  (defconstant +perf-count-sw-page-faults+ 34)
  (defconstant +perf-count-sw-context-switches+ 35)
  (defconstant +perf-count-sw-cpu-migrations+ 36)
  (defconstant +perf-count-sw-page-faults-min+ 37)
  (defconstant +perf-count-sw-page-faults-maj+ 38)

  (defconstant +perf-count-hw-all+
    '(+perf-count-hw-cpu-cycles+ +perf-count-hw-instructions+
      +perf-count-hw-cache-references+ +perf-count-hw-cache-misses+
      +perf-count-hw-branch-instructions+ +perf-count-hw-branch-misses+
      +perf-count-hw-bus-cycles+))

  (defconstant +perf-count-sw-all+
    '(+perf-count-sw-cpu-clock+ +perf-count-sw-task-clock+
      +perf-count-sw-page-faults+ +perf-count-sw-context-switches+
      +perf-count-sw-cpu-migrations+ +perf-count-sw-page-faults-min+
      +perf-count-sw-page-faults-maj+))

  (defconstant +perf-count-all+
    (append +perf-count-hw-all+ +perf-count-sw-all+))

;; Generalized hardware cache counters:

;;       { L1-D, L1-I, LLC, ITLB, DTLB, BPU } x
;;       { read, write, prefetch } x
;;       { accesses, misses }
  (defconstant +perf-count-hw-cache-l1d+ 0)
  (defconstant +perf-count-hw-cache-l1i+ 1)
  (defconstant +perf-count-hw-cache-ll+ 2)
  (defconstant +perf-count-hw-cache-dtlb+ 3)
  (defconstant +perf-count-hw-cache-itlb+ 4)
  (defconstant +perf-count-hw-cache-bpu+ 5)

  (defconstant +perf-count-format-string+
    '(("~@[~:D CPU cycle~:P consumed~%~]"
       "~@[~:D instruction~:P executed~%~]"
       "~@[~:D cache hit~:P~%~]"
       "~@[~:D cache misses~%~]"
       "~@[~:D branch instruction~:P~%~]"
       "~@[~:D branch misses~:P~%~]"
       "~@[~:D bus cycle~:P~%~]")
      ("~@[~:D cpu clock~:P~%~]"
       "~@[~:D task clock~:P~%~]"
       "~@[~:D page fault~:P~%~]"
       "~@[~:D context switch~:P~%~]"
       "~@[~:D cpu migration~:P~%~]"
       "~@[~:D minor fault~%~]"
       "~@[~:D major fault~:P~%~]")
      ()
      ()
      )))

;; Bits that can be set in hw_event.read_format to request that
;; reads on the counter should return the indicated quantities,
;; in increasing order of bit value, after the counter value.
(defcenum perf-counter-read-format
  (:perf-format-total-time-enabled)
  (:perf-format-total-time-running))

;; Bits that can be set in hw_event.record_type to request information
;; in the overflow packets.
(defcenum perf-counter-record-format
  (:perf-record-ip 1)
  (:perf-record-tid 2)
  (:perf-record-time 4)
  (:perf-record-addr 8)
  (:perf-record-group 16)
  (:perf-record-callchain 32))

(defcvar "errno" :int)

(defun fail (func)
  (error "~A failed: ~A" func
	 (foreign-funcall "strerror" :int *errno* :string)))

(defun perf-counter-open (event-type event)
  (with-foreign-object (attr 'perf-counter-attr)
    (foreign-funcall "memset" :pointer attr :int 0
		     :unsigned-long (foreign-type-size 'perf-counter-attr) :int)
    (with-foreign-slots ((type size config flags) attr perf-counter-attr)
      (setf type event-type
	    size (foreign-type-size 'perf-counter-attr)
	    config event
	    flags 3))
    (multiple-value-bind (ret)
	(foreign-funcall "syscall" :int +syscall+ :pointer attr :unsigned-long 0
			 :int -1 :int -1 :unsigned-long 0 :int)
      (when (< ret 0)
	(fail "Syscall perf_counter_open"))
      ret)))

(defun perf-counter-prctl (opt)
  (multiple-value-bind (ret)
      (foreign-funcall "prctl" :int opt :int)
    (when (< ret 0)
      (fail "prctl"))
    ret))

(defun perf-counters-start ()
  (perf-counter-prctl +pr-task-perf-counters-enable+))

(defun perf-counters-stop ()
  (perf-counter-prctl +pr-task-perf-counters-disable+))

(defun perf-counter-read (fd)
  (with-foreign-object (counter :unsigned-long)
    (multiple-value-bind (ret)
	(foreign-funcall "read" :int fd :pointer counter
			 :unsigned-long (foreign-type-size :unsigned-long)
			 :unsigned-long)
      (when (< ret (foreign-type-size :unsigned-long))
	(fail "perf-counter-read")))
    (mem-ref counter :unsigned-long)))

(defun perf-counter-close (fd)
  (multiple-value-bind (ret)
      (foreign-funcall "close" :int fd :int)
    (when (< ret 0)
      (fail "perf-cunter-close"))
    ret))

(defun perf-counters-open (counters)
  (let ((sym (car counters)))
    (when (symbolp sym)
      (case sym
	(:perf-count-hw-all (setq counters +perf-count-hw-all+))
	(:perf-count-sw-all (setq counters +perf-count-sw-all+))
	(:perf-count-all (setq counters +perf-count-all+))))
    (loop for i in counters
       for j = (or (and (symbolp i) (symbol-value i)) i)
       for (type . counter) = (if (< j 32)
				  (cons +perf-type-hardware+ j)
				  (cons +perf-type-software+ (- j 32)))
       collect (perf-counter-open type counter) into descriptors
       collect (nth counter (nth type +perf-count-format-string+)) into formats
       finally (return (list descriptors formats)))))

(defun perf-counters-read-and-close (descriptors)
  (loop for counter in descriptors
     collect (perf-counter-read counter)
     do (perf-counter-close counter)))

(defmacro with-performance-counters (cntrs &body body)
  (let ((counters (gensym)))
    `(let* ((,counters ',cntrs)
	    (x (perf-counters-open ,counters)))
       (perf-counters-start)
       ,@body
       (perf-counters-stop)
       (values (perf-counters-read-and-close (car x)) (cadr x)))))

(defmacro time+ (&body body)
  `(multiple-value-bind (vals formats)
       (with-performance-counters (:perf-count-all) ,@body)
     (apply #'format *trace-output*
	    (format nil "~A~{ ~A~}~A"
		    "~&Performance monitor:~%~@<~@;" formats "~:>~%")
	    vals)
     vals))

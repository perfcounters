;;;; Copyright 2009 Vitaly Mayatskikh <v.mayatskih@gmail.com>
;;;;
;;;; This file is a part of CL-Perfcounters
;;;;
;;;; Performance counters are special hardware registers available on most modern
;;;; CPUs. These registers count the number of certain types of hw events: such
;;;; as instructions executed, cachemisses suffered, or branches mis-predicted -
;;;; without slowing down the kernel or applications. These registers can also
;;;; trigger interrupts when a threshold number of events have passed - and can
;;;; thus be used to profile the code that runs on that CPU.
;;;;
;;;; The Linux Performance Counter subsystem provides an abstraction of these
;;;; hardware capabilities. It provides per task and per CPU counters, counter
;;;; groups, and it provides event capabilities on top of those.  It
;;;; provides "virtual" 64-bit counters, regardless of the width of the
;;;; underlying hardware counters.
;;;;
;;;; CL-Perfcounters is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; CL-Perfcounters is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defpackage #:perfcounters-system
  (:use #:cl #:asdf))

(in-package #:perfcounters-system)

(defsystem perfcounters
  :depends-on (#:cffi)
  :version "1.0"
  :components ((:file "perfcounters")))

(defmethod perform :after ((o load-op) (c (eql (find-system :perfcounters))))
  (provide 'perfcounters))
